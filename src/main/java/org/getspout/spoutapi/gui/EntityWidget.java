package org.getspout.spoutapi.gui;

public interface EntityWidget extends Widget {
	public void setEntityID(int entity);
	public int getEntityID();
}
